this app using API .NET in https://gitlab.com/claudius882/netapiapp

## Install

- run npm install in project folder
- for android, rename 'local(example).properties' to 'local.properties' in android folder and replace content with your android sdk path
- rename 'environment(example).js' to 'environment.js' in app/configs folder and replace content with your server API URL

## Development

- (for android) npm run android (real device via usb cable)
- (for apple) sudo gem install cocoapods first

## Build

- cd android
- run
  > `\* \* \* \* \* ./gradlew assembleRelease'
