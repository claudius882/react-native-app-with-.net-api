import {Environment} from './environment';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const server = axios.create({
  baseURL: Environment.server,
});
server.interceptors.request.use(
  async config => {
    const userData = await AsyncStorage.getItem(
      Environment.storageName || 'ReactStorageName',
    );
    let tokenData = null;
    if (userData) {
      tokenData = userData;
    }
    config.headers = {
      Accept: '*/*',
      Authorization: 'Bearer ' + tokenData,
    };
    return config;
  },
  error => {
    Promise.reject(error);
  },
);

export default server;
