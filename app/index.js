//import liraries
import React, {Component} from 'react';
import {LogBox} from 'react-native';
import Main from './navigations';
import {Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import AppReducer from './store/store/application';
// create a component
LogBox.ignoreAllLogs(true);
const Index = props => {
  const rootReducers = combineReducers({application: AppReducer});
  const store = createStore(rootReducers, applyMiddleware(ReduxThunk));
  return (
    <Provider store={store}>
      <Main />
    </Provider>
  );
};

export default Index;
