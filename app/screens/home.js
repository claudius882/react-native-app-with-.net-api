//import liraries
import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import * as AppAction from '../store/action/AppAction';
import {launchImageLibrary} from 'react-native-image-picker';

const Home = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);
  const isLogin = useSelector(state => state.application.isLogin);

  const setStatusLogin = () => {
    const action = AppAction.setStatusLogin();
    dispatch(action);
  };

  useEffect(() => {
    setStatusLogin();
  }, []);
  const logout = () => {
    const action = AppAction.logout();
    dispatch(action);
  };
  const uploadPhoto = async file => {
    if (file.didCancel) {
      return;
    }
    setLoading(true);
    setErrorMessage(null);
    setSuccessMessage(null);
    const action = AppAction.uploadPhoto(file.assets[0]);
    try {
      await dispatch(action).then(data => {
        console.log(data);
      });
      setSuccessMessage('Berhasil Upload data');
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setSuccessMessage(false);
      setErrorMessage(error.response.data.errors || error.message);
    }
  };
  const selectImage = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        maxWidth: 1280,
        maxHeight: 1280,
        quality: 0.8,
      },
      file => {
        uploadPhoto(file);
      },
    );
  };
  return (
    <SafeAreaView style={[styles.safeAreaView]}>
      {isLogin && (
        <View style={styles.homeHeader}>
          <FontAwesome5
            onPress={() => navigation.push('Profile')}
            name="user"
            style={[styles.colorPrimary]}
            size={24}
          />
          <FontAwesome5
            onPress={logout}
            name="sign-out-alt"
            style={[styles.colorDanger, {marginLeft: 20}]}
            size={24}
          />
        </View>
      )}
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View
            style={{
              marginTop: isLogin ? 0 : 50,
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Text style={[styles.header]}>Selamat Datang</Text>
          </View>

          {isLogin ? (
            <View style={styles.homeContainer}>
              {errorMessage && typeof errorMessage === 'string' && (
                <View style={{width: '100%'}}>
                  <Text style={styles.colorDanger}>{errorMessage}</Text>
                </View>
              )}
              <TouchableOpacity
                disabled={loading}
                onPress={selectImage}
                style={[styles.btnPrimary]}>
                {loading ? (
                  <ActivityIndicator size="small" color="white" />
                ) : (
                  <Text style={styles.whiteColor}>Upload Gambar</Text>
                )}
              </TouchableOpacity>
              {successMessage && (
                <Text style={styles.colorSuccess}>{successMessage}</Text>
              )}
            </View>
          ) : (
            <View style={styles.homeContainer}>
              <Text style={[styles.body1]}>Anda belum login</Text>
              <TouchableOpacity
                onPress={() => navigation.push('Login')}
                style={[styles.btnPrimary]}>
                <Text style={[styles.body2, styles.whiteColor]}>
                  Login disini
                </Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

//make this component available to the app
export default Home;
