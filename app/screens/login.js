//import liraries
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import * as AppAction from '../store/action/AppAction';
import styles from './styles';
// create a component
const Login = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const [showPassword, setShowPassword] = useState(true);
  const [passwordInput, setPasswordInput] = useState(null);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const isLogin = useSelector(state => state.application.isLogin);
  const [input, setInput] = useState({
    email: '',
    password: '',
  });

  const loginHandler = async () => {
    if (!input.email || !input.password) {
      return;
    }
    setLoading(true);
    setErrorMessage(null);
    const action = AppAction.login(input);
    try {
      await dispatch(action);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setErrorMessage(error.response.data.errors);
    }
  };
  useEffect(() => {
    if (isLogin) {
      navigation.navigate('Home');
    }
  }, [isLogin]);
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'android' ? 'height' : 'padding'}
      style={styles.keyboardAvoidingView}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.container]}>
          <View style={{marginTop: 50, alignItems: 'center', marginBottom: 10}}>
            <Text style={[styles.headline, styles.bold]}>Login</Text>
          </View>
          <View style={[styles.homeContainer, {paddingHorizontal: 20}]}>
            {errorMessage && typeof errorMessage === 'string' && (
              <View style={{width: '100%'}}>
                <Text style={styles.colorDanger}>{errorMessage}</Text>
              </View>
            )}
            {errorMessage && errorMessage.constructor.name === 'Array' && (
              <View style={{width: '100%'}}>
                {errorMessage.map((item, index) => {
                  return (
                    <Text key={index.toString()} style={styles.colorDanger}>
                      {item}
                    </Text>
                  );
                })}
              </View>
            )}
            <View style={styles.textInputContainer}>
              <TextInput
                onChangeText={text => setInput({...input, email: text})}
                style={styles.textInput}
                placeholder="Email"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                blurOnSubmit={false}
                value={input.email}
                onSubmitEditing={() => {
                  passwordInput.focus();
                }}
              />
              {errorMessage &&
                typeof errorMessage === 'object' &&
                errorMessage.Email && (
                  <View style={{width: '100%'}}>
                    {errorMessage.Email.map((item, index) => {
                      return (
                        <Text key={index.toString()} style={styles.colorDanger}>
                          {item}
                        </Text>
                      );
                    })}
                  </View>
                )}
            </View>
            <View
              style={[
                styles.textInputContainer,
                {
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                },
              ]}>
              <TextInput
                ref={input => {
                  setPasswordInput(input);
                }}
                onChangeText={text => setInput({...input, password: text})}
                style={[styles.textInput, {flex: 1}]}
                placeholder="Password"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="default"
                returnKeyType="go"
                secureTextEntry={showPassword}
                blurOnSubmit={true}
                value={input.password}
                onSubmitEditing={loginHandler}
              />
              {errorMessage &&
                typeof errorMessage === 'object' &&
                errorMessage.Password && (
                  <View style={{width: '100%'}}>
                    {errorMessage.Password.map((item, index) => {
                      return (
                        <Text key={index.toString()} style={styles.colorDanger}>
                          {item}
                        </Text>
                      );
                    })}
                  </View>
                )}
              <TouchableOpacity
                onPress={() => setShowPassword(!showPassword)}
                style={{marginTop: 10, marginLeft: -35}}>
                <Text>{showPassword ? 'Show' : 'Hide'}</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              disabled={loading}
              onPress={loginHandler}
              style={[styles.btnPrimary]}>
              {loading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Text style={styles.whiteColor}>Login</Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.push('Register')}
              style={[styles.btnPrimaryOutline]}>
              <Text style={styles.colorPrimary}>Daftar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

//make this component available to the app
export default Login;
