//import liraries
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import * as AppAction from '../store/action/AppAction';
// create a component
const Profile = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const [emailInput, setEmailInput] = useState(null);
  const [phoneInput, setPhoneInput] = useState(null);
  const [load, setLoad] = useState(false);
  const [errorLoad, setErrorLoad] = useState(null);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);
  const isLogin = useSelector(state => state.application.isLogin);
  const dataUser = useSelector(state => state.application.dataUser);

  const [input, setInput] = useState({
    username: '',
    email: '',
    phonenumber: '',
  });
  useEffect(() => {
    if (!isLogin) {
      navigation.navigate('Home');
    }
  }, []);
  useEffect(() => {
    if (dataUser) {
      setInput({
        ...input,
        username: dataUser.username || '',
        email: dataUser.email || '',
        phonenumber: dataUser.phoneNumber || '',
      });
    }
  }, [dataUser]);

  useEffect(() => {
    loadUser();
  }, []);
  const loadUser = async () => {
    setLoad(true);
    setErrorLoad(null);
    const action = AppAction.getProfile();
    try {
      await dispatch(action);
      setLoad(false);
    } catch (error) {
      setLoad(false);
      setErrorLoad(error.message);
    }
  };
  const submithandler = async () => {
    if (!input.email || !input.username) {
      return;
    }
    setLoading(true);
    setErrorMessage(null);
    setSuccessMessage(null);
    const action = AppAction.updateProfile(input);
    try {
      await dispatch(action);
      setLoading(false);
      setSuccessMessage('Update berhasil');
    } catch (error) {
      setLoading(false);
      setSuccessMessage(null);
      setErrorMessage(error.response.data.errors || error.message);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'android' ? 'height' : 'padding'}
      style={styles.keyboardAvoidingView}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.container]}>
          <View style={{marginTop: 50, alignItems: 'center', marginBottom: 10}}>
            <Text style={[styles.headline, styles.bold]}>Profile</Text>
          </View>
          {load && (
            <View style={{marginTop: 10, alignItems: 'center'}}>
              <ActivityIndicator size="large" color={styles.colorPrimary} />
              <Text style={styles.body1}>Loading</Text>
            </View>
          )}
          {!load && errorLoad && (
            <View style={{marginTop: 10, alignItems: 'center'}}>
              <Text style={styles.colorDanger}>{errorLoad.toString()}</Text>
            </View>
          )}
          {!load && !errorLoad && (
            <View style={[styles.homeContainer, {paddingHorizontal: 20}]}>
              {errorMessage && typeof errorMessage === 'string' && (
                <View style={{width: '100%'}}>
                  <Text style={styles.colorDanger}>{errorMessage}</Text>
                </View>
              )}
              {errorMessage && errorMessage.constructor.name === 'Array' && (
                <View style={{width: '100%'}}>
                  {errorMessage.map((item, index) => {
                    return (
                      <Text key={index.toString()} style={styles.colorDanger}>
                        {item}
                      </Text>
                    );
                  })}
                </View>
              )}
              <View style={styles.textInputContainer}>
                <TextInput
                  onChangeText={text => setInput({...input, username: text})}
                  style={styles.textInput}
                  placeholder="User name"
                  autoCorrect={false}
                  autoCapitalize="none"
                  keyboardType="default"
                  returnKeyType="next"
                  blurOnSubmit={false}
                  value={input.username}
                  onSubmitEditing={() => {
                    emailInput.focus();
                  }}
                />
                {errorMessage &&
                  typeof errorMessage === 'object' &&
                  errorMessage.UserName && (
                    <View style={{width: '100%'}}>
                      {errorMessage.UserName.map((item, index) => {
                        return (
                          <Text
                            key={index.toString()}
                            style={styles.colorDanger}>
                            {item}
                          </Text>
                        );
                      })}
                    </View>
                  )}
              </View>
              <View style={styles.textInputContainer}>
                <TextInput
                  ref={input => {
                    setEmailInput(input);
                  }}
                  onChangeText={text => setInput({...input, email: text})}
                  style={styles.textInput}
                  placeholder="Email"
                  autoCorrect={false}
                  autoCapitalize="none"
                  keyboardType="email-address"
                  returnKeyType="next"
                  blurOnSubmit={false}
                  value={input.email}
                  onSubmitEditing={() => {
                    phoneInput.focus();
                  }}
                />
                {errorMessage &&
                  typeof errorMessage === 'object' &&
                  errorMessage.Email && (
                    <View style={{width: '100%'}}>
                      {errorMessage.Email.map((item, index) => {
                        return (
                          <Text
                            key={index.toString()}
                            style={styles.colorDanger}>
                            {item}
                          </Text>
                        );
                      })}
                    </View>
                  )}
              </View>
              <View style={styles.textInputContainer}>
                <TextInput
                  ref={input => {
                    setPhoneInput(input);
                  }}
                  onChangeText={text => setInput({...input, phonenumber: text})}
                  style={styles.textInput}
                  placeholder="Telepon"
                  autoCorrect={false}
                  autoCapitalize="none"
                  keyboardType="default"
                  returnKeyType="next"
                  blurOnSubmit={true}
                  value={input.phonenumber}
                  onSubmitEditing={submithandler}
                />
                {errorMessage &&
                  typeof errorMessage === 'object' &&
                  errorMessage.PhoneNumber && (
                    <View style={{width: '100%'}}>
                      {errorMessage.PhoneNumber.map((item, index) => {
                        return (
                          <Text
                            key={index.toString()}
                            style={styles.colorDanger}>
                            {item}
                          </Text>
                        );
                      })}
                    </View>
                  )}
              </View>
              <TouchableOpacity
                disabled={loading}
                onPress={submithandler}
                style={[styles.btnPrimary]}>
                {loading ? (
                  <ActivityIndicator size="small" color="white" />
                ) : (
                  <Text style={styles.whiteColor}>Update Profile</Text>
                )}
              </TouchableOpacity>
              {successMessage && (
                <Text style={styles.colorSuccess}>{successMessage}</Text>
              )}
            </View>
          )}
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

//make this component available to the app
export default Profile;
