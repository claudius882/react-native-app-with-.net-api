//import liraries
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {useDispatch, useSelector} from 'react-redux';
import * as AppAction from '../store/action/AppAction';
// create a component
const Register = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);
  const [showPassword, setShowPassword] = useState(true);
  const [emailInput, setEmailInput] = useState(null);
  const [passwordInput, setPasswordInput] = useState(null);
  const isLogin = useSelector(state => state.application.isLogin);
  const [input, setInput] = useState({
    username: '',
    email: '',
    password: '',
  });
  useEffect(() => {
    if (isLogin) {
      navigation.navigate('Home');
    }
  }, [isLogin]);
  const registerhandler = async () => {
    if (!input.username || !input.email || !input.password) {
      return;
    }
    setLoading(true);
    setErrorMessage(null);
    setSuccessMessage(null);
    const action = AppAction.register(input);
    try {
      await dispatch(action);
      setLoading(false);
      setSuccessMessage('Pendaftaran berhasil');
      resetInput();
    } catch (error) {
      setLoading(false);
      setSuccessMessage(false);
      setErrorMessage(error.response.data.errors);
    }
  };
  const resetInput = () => {
    setInput({
      ...input,
      username: '',
      email: '',
      password: '',
    });
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'android' ? 'height' : 'padding'}
      style={styles.keyboardAvoidingView}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[styles.container]}>
          <View style={{marginTop: 50, alignItems: 'center', marginBottom: 10}}>
            <Text style={[styles.headline, styles.bold]}>Registrasi</Text>
          </View>
          <View style={[styles.homeContainer, {paddingHorizontal: 20}]}>
            {errorMessage && typeof errorMessage === 'string' && (
              <View style={{width: '100%'}}>
                <Text style={styles.colorDanger}>{errorMessage}</Text>
              </View>
            )}
            {errorMessage && errorMessage.constructor.name === 'Array' && (
              <View style={{width: '100%'}}>
                {errorMessage.map((item, index) => {
                  return (
                    <Text key={index.toString()} style={styles.colorDanger}>
                      {item}
                    </Text>
                  );
                })}
              </View>
            )}
            <View style={styles.textInputContainer}>
              <TextInput
                onChangeText={text => setInput({...input, username: text})}
                style={styles.textInput}
                placeholder="User name"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="default"
                returnKeyType="next"
                blurOnSubmit={false}
                value={input.username}
                onSubmitEditing={() => {
                  emailInput.focus();
                }}
              />
              {errorMessage &&
                typeof errorMessage === 'object' &&
                errorMessage.UserName && (
                  <View style={{width: '100%'}}>
                    {errorMessage.UserName.map((item, index) => {
                      return (
                        <Text key={index.toString()} style={styles.colorDanger}>
                          {item}
                        </Text>
                      );
                    })}
                  </View>
                )}
            </View>
            <View style={styles.textInputContainer}>
              <TextInput
                ref={input => {
                  setEmailInput(input);
                }}
                onChangeText={text => setInput({...input, email: text})}
                style={styles.textInput}
                placeholder="Email"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                blurOnSubmit={false}
                value={input.email}
                onSubmitEditing={() => {
                  passwordInput.focus();
                }}
              />
              {errorMessage &&
                typeof errorMessage === 'object' &&
                errorMessage.Email && (
                  <View style={{width: '100%'}}>
                    {errorMessage.Email.map((item, index) => {
                      return (
                        <Text key={index.toString()} style={styles.colorDanger}>
                          {item}
                        </Text>
                      );
                    })}
                  </View>
                )}
            </View>
            <View
              style={[
                styles.textInputContainer,
                {
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                },
              ]}>
              <TextInput
                ref={input => {
                  setPasswordInput(input);
                }}
                onChangeText={text => setInput({...input, password: text})}
                style={[styles.textInput, {flex: 1}]}
                placeholder="Password"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="default"
                returnKeyType="go"
                secureTextEntry={showPassword}
                blurOnSubmit={true}
                value={input.password}
                onSubmitEditing={registerhandler}
              />

              <TouchableOpacity
                onPress={() => setShowPassword(!showPassword)}
                style={{marginTop: 10, marginLeft: -35}}>
                <Text>{showPassword ? 'Show' : 'Hide'}</Text>
              </TouchableOpacity>
            </View>
            {errorMessage &&
              typeof errorMessage === 'object' &&
              errorMessage.Password && (
                <View style={{width: '100%'}}>
                  {errorMessage.Password.map((item, index) => {
                    return (
                      <Text key={index.toString()} style={styles.colorDanger}>
                        {item}
                      </Text>
                    );
                  })}
                </View>
              )}
            <TouchableOpacity
              disabled={loading}
              onPress={registerhandler}
              style={[styles.btnPrimary]}>
              {loading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <Text style={styles.whiteColor}>Daftar</Text>
              )}
            </TouchableOpacity>
            {successMessage && (
              <Text style={styles.colorSuccess}>{successMessage}</Text>
            )}
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

//make this component available to the app
export default Register;
