import {StyleSheet} from 'react-native';
import * as Utils from '@utils';
export default StyleSheet.create({
  safeAreaView: {
    flex: 1,
    backgroundColor: 'white',
  },
  keyboardAvoidingView: {
    flex: 1,
    backgroundColor: '#fff',
    position: 'relative',
  },

  containerScreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  container: {padding: 20},
  homeContainer: {
    borderWidth: 1,
    padding: 10,
    maxWidth: 400,
    borderRadius: 8,
    marginVertical: 10,
    borderColor: '#BDBDBD',
    alignItems: 'center',
  },
  homeHeader: {
    minHeight: 50,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    justifyContent: 'flex-end',
  },
  textInputContainer: {width: '100%', marginBottom: 20},
  textInput: {
    borderBottomWidth: 1,
    borderBottomColor: '#BDBDBD',
    fontSize: 16,
  },
  colorSuccess: {
    color: '#22B574',
  },
  colorDanger: {
    color: '#FF0000',
  },
  colorWarning: {
    color: '#eda400',
  },
  colorPrimary: {
    color: '#3C5A99',
  },
  whiteColor: {
    color: '#fff',
  },
  btnPrimary: {
    backgroundColor: '#3C5A99',
    padding: 10,
    width: '100%',
    marginVertical: 10,
    alignItems: 'center',
    borderRadius: 5,
  },
  btnPrimaryOutline: {
    borderColor: '#3C5A99',
    borderWidth: 1,
    padding: 10,
    width: '100%',
    marginVertical: 10,
    alignItems: 'center',
    borderRadius: 5,
  },
  btnSuccess: {
    backgroundColor: '#22B574',
    padding: 10,
    width: '100%',
    marginVertical: 10,
    alignItems: 'center',
    borderRadius: 5,
  },
  btnDanger: {
    backgroundColor: '#FF0000',
    padding: 10,
    width: '100%',
    marginVertical: 10,
    alignItems: 'center',
    borderRadius: 5,
  },
  btnWarning: {
    backgroundColor: '#eda400',
    padding: 10,
    width: '100%',
    marginVertical: 10,
    alignItems: 'center',
    borderRadius: 5,
  },
  header: {
    fontSize: 34,
  },
  title1: {
    fontSize: 28,
  },
  title2: {
    fontSize: 22,
  },
  title3: {
    fontSize: 20,
  },
  headline: {
    fontSize: 19,
  },
  body1: {
    fontSize: 17,
  },
  bodiest: {
    fontSize: 16,
  },
  body2: {
    fontSize: 14,
  },
  callout: {
    fontSize: 17,
  },
  subhead: {
    fontSize: 15,
  },
  footnote: {
    fontSize: 13,
  },
  caption1: {
    fontSize: 12,
  },
  caption2: {
    fontSize: 11,
  },
  overline: {
    fontSize: 10,
  },
  overline1: {
    fontSize: 9,
  },
  overline2: {
    fontSize: 8,
  },
  bold: {
    fontWeight: '700',
  },
  strikeText: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
});
