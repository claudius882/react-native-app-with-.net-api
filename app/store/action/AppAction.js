import * as Action from '../actionTypes';
import {Environment, Api} from '@configs';
import AsyncStorage from '@react-native-community/async-storage';

export const setStatusLogin = () => {
  return async dispatch => {
    const dataStorage = await AsyncStorage.getItem(
      Environment.storageName || 'ReactStorageName',
    );
    if (dataStorage) {
      dispatch({type: Action.APP_SET_LOGIN_STATUS, status: true});
    } else {
      dispatch({type: Action.APP_SET_LOGIN_STATUS, status: false});
    }
  };
};
export const logout = () => {
  AsyncStorage.removeItem(Environment.storageName || 'ReactStorageName');
  return dispatch => {
    dispatch(setStatusLogin());
  };
};
export const login = input => {
  return async dispatch => {
    const data = {
      email: input.email,
      password: input.password,
    };
    const response = await Api.post(`/api/auth/login`, data);
    const dataResponse = response.data;
    saveUserLogin(dataResponse.token);
    dispatch({type: Action.APP_SET_LOGIN_STATUS, status: true});
    return true;
  };
};
export const register = input => {
  return async dispatch => {
    const data = {
      username: input.username,
      email: input.email,
      password: input.password,
    };
    const response = await Api.post(`/api/auth/register`, data);
    return true;
  };
};
const saveUserLogin = async token => {
  const storageName = Environment.storageName || 'ReactStorageName';
  await AsyncStorage.setItem(storageName, token);
};

export const getProfile = () => {
  return async dispatch => {
    const response = await Api.get(`/api/user`);
    const dataResponse = response.data.data;
    const userData = {
      username: dataResponse.userName,
      email: dataResponse.email,
      phoneNumber: dataResponse.phoneNumber,
    };
    dispatch({type: Action.APP_SET_USER_DATA, data: userData});
  };
};
export const updateProfile = input => {
  return async dispatch => {
    const data = {
      username: input.username,
      email: input.email,
      phonenumber: input.phonenumber,
    };
    await Api.post(`/api/user`, data);
    return true;
  };
};
export const uploadPhoto = file => {
  return async dispatch => {
    const data = new FormData();
    data.append('image', {
      uri: file.uri,
      type: file.type,
      name: file.fileName,
    });
    const response = await Api.post(`/api/upload`, data);
    const dataResponse = response.data.data;
    return dataResponse;
  };
};
