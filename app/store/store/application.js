import * as Action from '../actionTypes';

const initialState = {
  isLogin: false,
  dataUser: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Action.APP_SET_LOGIN_STATUS:
      return {
        ...state,
        isLogin: action.status,
      };
      break;
    case Action.APP_SET_USER_DATA:
      return {
        ...state,
        dataUser: action.data,
      };
      break;

    default:
      return state;
      break;
  }
};
