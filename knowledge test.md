## Knowledge test

1. Coba jelaskan salah satu kasus dimana kamu menggunakan “await”.

   - penggunaan "await" selalu disertai dengan "async". biasanya saya gunakan untuk menunggu eksekusi code selesai dijalankan kemudian mengksekusi code/baris selanjutnya. salah satu contohnya :

   ```const registerhandler = async () => {
       if (!input.username || !input.email || !input.password) {
           return;
        }
       setLoading(true);
       setErrorMessage(null); s
       etSuccessMessage(null);
       const action = AppAction.register(input);
       try {
           await dispatch(action);
           setLoading(false);
           setSuccessMessage('Pendaftaran berhasil');
           resetInput();
        } catch (error) {
            setLoading(false);
            setSuccessMessage(false);
            setErrorMessage(error.response.data.errors);
        }};
   ```

   pada contoh diatas, saya menggunakan `await` pada `dispatch(action)` karena ingin menunggu proses dispatch selesai dijalankan/eksekusi kemudian lanjut `setLoading(false)` (mematikan state loading);

2. Sebutkan salah satu contoh penggunaan local storage
   - penggunaan local storage biasa saya gunakan untuk menyimpan data kecil seperti token api,yang mana data token ini berguna mengidentifikasi pengguna yang melakukan request ke server
